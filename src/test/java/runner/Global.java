package runner;

/**
 * Created by Rahul on 9/03/2017.
 */
public class Global {

    public static String GlobalWebsiteUrl = "https://www.unilever.com";
    public static String NativeWebsiteUrl = "https://www.unilever.com";
    public static String GlobalSearchString = "Surf";
    public static String NativeSearchString = "Surf";
}
