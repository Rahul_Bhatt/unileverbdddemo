package runner;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rahul on 9/3/2017.
 */
public class WebBrowser extends Global {

    public static WebDriver driver;
    public static Properties prop;

    @Before
    public static void Initialize() {

        System.setProperty("webdriver.gecko.driver", "E:/Global_Automation/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(60000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(60000, TimeUnit.MILLISECONDS);
    }

    public static void ReadData() {

        prop = new Properties();
        try {
            prop.load(new FileInputStream("E:\\Global_Automation\\comunileversearchbdd\\src\\test\\resources\\Data\\data.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @After
    public static void TearDown() {
        driver.quit();
    }
}

