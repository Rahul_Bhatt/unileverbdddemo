package runner;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static runner.WebBrowser.driver;
import static runner.WebBrowser.prop;

/**
 * Created by Rahul on 9/03/2017.
 */
public class Extension {


    //NAVIGATE TO GLOBAL URL
    public void NavigateToUrl() {


        if (driver == null) {
            System.setProperty("webdriver.gecko.driver", "E:/Global_Automation/geckodriver.exe");
            driver = new FirefoxDriver();
        }
        WebBrowser.ReadData();
        driver.navigate().to(prop.getProperty("GlobalURL"));
    }

    //NAVIGATE TO LOCAL URL
    public void NavigateToLocalUrl() {


        if (driver == null) {
            System.setProperty("webdriver.gecko.driver", "E:/Global_Automation/geckodriver.exe");
            driver = new FirefoxDriver();
        }
        WebBrowser.ReadData();
        driver.navigate().to(prop.getProperty("LocalURL"));
    }


    //WAIT UNTIL ELEMENT FOUND OR LOAD WITHIN PAGE
    public void WaitUntilIsElementExistsAndDisplayed(By Locator) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(ExpectedConditions.visibilityOfElementLocated(Locator));
            wait.until(ExpectedConditions.presenceOfElementLocated(Locator));
        } catch (Exception ex) {
            Assert.fail("Following element is not displayed : " + Locator);
        }
    }

    //CLICK ON SINGLE ELEMENT
    public void Click(By Locator) {
        WaitUntilIsElementExistsAndDisplayed(Locator);
        driver.findElement(Locator).click();
    }

    //CLICK ON PARTICULAR ELEMENT IF MULTIPLE ELEMENTS FOUND
    public void FirstClick(By Locator, int index) {
        WaitUntilIsElementExistsAndDisplayed(Locator);
        //System.out.println("SIZE : " + driver.findElements(By.cssSelector(".search")).size());
        driver.findElements(Locator).get(index).click();

    }

    //CLEAR FIELDS
    public void Clear(By Locator) {
        WaitUntilIsElementExistsAndDisplayed(Locator);
        driver.findElement(Locator).clear();
    }

    //FIND ELEMENTS
    public void SendKeys(By Locator, String text) {
        Clear(Locator);
        driver.findElement(Locator).sendKeys(text);
    }


    //CHECKING RESULT IN ENGLISH LANGUAGE IF USER IS ON GLOBAL URL
    public void AssertGlobalResult() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Assert.assertTrue(driver.findElements(By.xpath("//div[@class=\"brands\"]//a/@href")).size()>0);
        if (driver.findElements(By.xpath("//div[@class=\"brands\"]//a/@href")).size() > 0) {
            System.out.println("Search By English keyword seems working and result display brands as well as result");
        } else {
            Assert.fail("Search by keyword did not return any result in English language");

        }
    }

    //CHECKING RESULT IN LOCAL LANGUAGE IF USER IS ON LOCAL URL
    public void AssertLocalResult() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<WebElement> SearchTitle = driver.findElements(By.xpath(WebBrowser.prop.getProperty("EnglishLanguageTitle")));
        if (SearchTitle.size() > 0) {
            Assert.fail("Search by keyword did not return any result in local language OR local language search title = search");

        } else {
            System.out.println("Search By local langiage with string surf seems working and result display in local language");

        }
    }
}
