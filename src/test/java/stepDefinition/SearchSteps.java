package stepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.gl.E;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import runner.Extension;
import runner.Global;
import runner.WebBrowser;

import java.util.List;

/**
 * Created by Rahul on 8/25/2017.
 */
public class SearchSteps extends Global {

    WebDriver driver = WebBrowser.driver;
    Extension extension = new Extension();


    @Given("^I am on the unilever home page$")
    public void iAmOnTheUnileverHomePage() throws Throwable {
        extension.NavigateToUrl();
    }

    @When("^I select to search for surf in English language$")
    public void iSelectToSearchForSurf() throws Throwable {

        extension.FirstClick(By.cssSelector(".search"), 0);
        extension.SendKeys(By.name("search"), WebBrowser.prop.getProperty("EnglishSearchKeyword"));
        extension.Click(By.xpath("//button[@class='search-strip__submit']"));
    }


    @Then("^I get the search results with brand results at the top$")
    public void iGetTheSearchResultsWithBrandResultsAtTheTop() throws Throwable {
        extension.AssertGlobalResult();
    }

    @Given("^I am on the unilever home page and I change to local country")
    public void iAmOnTheUnileverHomePageAndSelectLocalCountry() throws Throwable {
        extension.NavigateToLocalUrl();
    }

    @When("^I select to search for surf in local language$")
    public void iSelectToSearchForSurfLocal() throws Throwable {

        extension.FirstClick(By.cssSelector(".search"), 0);
        extension.SendKeys(By.name("search"), WebBrowser.prop.getProperty("LocalSearchKeyword"));
        extension.Click(By.xpath("//button[@class='search-strip__submit']"));
    }

    @Then("^I get the search results in local language$")
    public void iGetTheSearchResultsInLocalLanguage() throws Throwable {
        extension.AssertLocalResult();
    }

}


