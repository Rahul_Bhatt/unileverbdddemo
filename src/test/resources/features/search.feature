Feature: search

  @new_test
  Scenario: Searching as a global user in English Language
    Given I am on the unilever home page
    When I select to search for surf in English language
    Then I get the search results with brand results at the top



    @new_test
    Scenario: Searching as local user in native language
      Given I am on the unilever home page and I change to local country
      When I select to search for surf in local language
      Then I get the search results in local language

